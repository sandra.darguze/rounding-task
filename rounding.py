#!/usr/bin/env python
# coding: utf-8

# In[6]:


def calculation(a, b):
    result = round((12 * a + 25 * b) / (1 + a**(2**b)), 2)
    return(result)

